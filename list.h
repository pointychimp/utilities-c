/*
 * Matt Traudt mt.traudt@gmail.com
 * bitcoin:1QDqv2QU98YpHdHqyc8xxpUNeZ8M2VbtKS
 * See my other utilities: https://bitbucket.org/pointychimp/utilities-c.git
 *
 * last modified: 2014-02-27
 *
 * README ----------------------------------------------------------------------
 *
 * This is a (somewhat poor) attempt at mirroring the
 * capabilities of c++'s built-in std::vector. See
 * http://www.cplusplus.com/reference/vector/vector/
 * for more information on std::vector
 *
 * This was put together as a personal lesson on exceptions and an exercise
 * on linked lists. I have done minimal testing of this. Feel free to use/modify 
 * however you like, but don't blame me if it blows up!
 *
 * LICENSE ---------------------------------------------------------------------
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
#ifndef LIST_H
#define LIST_H

#include <exception>
#include <stdexcept>

#define LIST_SUPPRESS_NONE      0x00
#define LIST_SUPPRESS_WARNINGS  0x01
#define LIST_SUPPRESS_ERRORS    0x02
//#define LIST_OPTIMIZE_PUSH_BACK 0x04
// 0x08// 0x10// 0x20// 0x40//0x80
#define LIST_SUPPRESS_ALL       LIST_SUPPRESS_WARNINGS | LIST_SUPPRESS_ERRORS

template <class T>
class List
{
    public:
              T& at(unsigned int i);
        const T& at(unsigned int i) const;
              T& operator[](unsigned int i);
        const T& operator[](unsigned int i) const;
        //      T& front()       {return first->value;} // need to handle when size = 0
        //const T& front() const {return first->value;}
        //      T& back();
        //const T& back() const;
        unsigned int size()   const noexcept {return length();}
        unsigned int length() const noexcept {return len;}
        void insertBefore(unsigned int i, T val);
        void insertAfter(unsigned int i, T val);
        void push(T val);
        void pop();
        void push_back(T val);
        void pop_back();
        bool empty() const noexcept {return (length()==0?true:false);}

        List<T>() {first = nullptr; len = 0; options = LIST_SUPPRESS_WARNINGS;}
        List<T>(T val, unsigned char ops = LIST_SUPPRESS_WARNINGS) {first = new Node(val); len = 1; options = ops;}
        ~List<T>() {}
    protected:
    private:
        struct Node
        {
            T value;
            Node* next;
            Node()                 {             next = nullptr;}
            Node(T val)            {value = val; next = nullptr;}
            Node(T val, Node* nxt) {value = val; next = nxt;}
        };
        Node* first;
        unsigned int len;
        unsigned char options;

        Node* end(); // advance to end of list and return reference to last item

        class ErrorOutOfRange  : public std::exception {const char* what() const noexcept {return "Error. Index out of range.";}} err_oor;
        class WarningEmptyList : public std::exception {const char* what() const noexcept {return "Warning. Empty list.";}}       wrn_empt;
        class ErrorEmptyList   : public std::exception {const char* what() const noexcept {return "Error. Empty list.";}}         err_empt;
};

template <class T>
T& List<T>::at(unsigned int i)
{
    Node* working = first;
    if ((i > length() || working == nullptr) && !(options & LIST_SUPPRESS_ERRORS)) throw err_oor;
    for ( ; i > 0 && working != nullptr; i--, working = working->next); // advance to node i
    if ((i > 0 || working == nullptr) && !(options & LIST_SUPPRESS_ERRORS)) throw err_oor;
    return working->value;
}
template <class T>
T& List<T>::operator[](unsigned int i)
{
    return at(i);
}
template <class T>
void List<T>::insertBefore(unsigned int i, T val)
{
    if      ((length() == 0)    && !(options & LIST_SUPPRESS_ERRORS)) throw err_empt;
    else if ((i > length() - 1) && !(options & LIST_SUPPRESS_ERRORS)) throw err_oor;
    else if (i == 0) {push(val);}
    else              insertAfter(i-1, val);
}
template <class T>
void List<T>::insertAfter(unsigned int i, T val)
{
    if      ((length() == 0)     && !(options & LIST_SUPPRESS_ERRORS)) throw err_empt;
    else if ((i  > length() - 1) && !(options & LIST_SUPPRESS_ERRORS)) throw err_oor;
    else if  (i == length() - 1) {push_back(val);}
    else
    {
        Node* working = first;
        for ( ; i > 0; i--, working = working->next);
        if (working->next != nullptr) working->next = new Node(val, working->next);
        else                          working->next = new Node(val);
        len++;
    }
}
template <class T>
void List<T>::push(T val)
{
    if (length() == 0 && !(options & LIST_SUPPRESS_WARNINGS)) throw wrn_empt;
    else
    {
        first = new Node(val, first);
        len++;
    }
}
template <class T>
void List<T>::pop()
{
    if      (length() == 0 && !(options & LIST_SUPPRESS_WARNINGS)) throw wrn_empt;
    else if (length() == 0)                                        return;
    else
    {
        Node* second = first->next;
        delete first;
        first = second;
        len--;
    }
}
template <class T>
void List<T>::push_back(T val)
{
    if      (length() == 0 && !(options & LIST_SUPPRESS_WARNINGS)) throw wrn_empt;
    else if (length() == 0) first       = new Node(val); // special case if empty list
    else                    end()->next = new Node(val); // otherwise, find end and tack on new node
    len++;
}
template <class T>
void List<T>::pop_back()
{
    if      (length() == 0 && !(options & LIST_SUPPRESS_WARNINGS)) throw wrn_empt;
    else if (length() == 0)                                        return;
    else if (length() == 1)                                       {delete first; first = nullptr; len--;}
    else
    {
        Node* working = first;
        for (unsigned int i = length(); i > 2; i--, working = working->next);
        delete working->next;
        working->next = nullptr;
        len--;
    }
}
template <class T>
typename List<T>::Node* List<T>::end()
{
    if (length() == 0 && !(options & LIST_SUPPRESS_ERRORS)) throw err_empt;
    else if (length() == 0) return nullptr;
    Node* working = first;
    for ( ; working->next != nullptr; working = working->next);
    return working;
}
#endif // LIST_H
