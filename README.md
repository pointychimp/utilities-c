This is a small, but probably growing, collection of libraries/headers/whatever that I've developed.

Some may be useful. Some may be redundant. I just hope someone finds them useful.

Side note: some of these utilities are interdependent and/or require c++11


* class List            semi-working linked-list class
* namespace NumStr      easily convert from int to str with special options
* class DateTime        store and manipulate dates/times